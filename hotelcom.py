import requests
from bs4 import BeautifulSoup
import pandas as pd

def menu(): 
    choice = '0'
    while choice == '0':
        print("[1] 屯門區") 
        print("[2] 沙田區") 
        choice = input ("請選擇地區酒店: ")
        print('')

    if choice == '1':
        transfer_Soup(tuenMum(check_In = (input("請輸入入住日期 YYYY-MM-DD: ")),check_Out = (input("請輸入出走住日期 YYYY-MM-DD: "))))
    elif choice == '2':
        transfer_Soup(shaTin(check_In = (input("請輸入入住日期 YYYY-MM-DD: ")),check_Out = (input("請輸入出走住日期 YYYY-MM-DD: "))))
    else:
        print("I don't understand your choice.") 

def tuenMum(check_In,check_Out):

        url = f'https://zh.hotels.com/search.do?destination-id=605978&q-check-in={check_In}&q-check-out={check_Out}&q-rooms=1&q-room-0-adults=1&q-room-0-children=0'
        # print(url)
        r = requests.get(url)
        soup = BeautifulSoup(r.content,'html.parser')
        return soup

def shaTin(check_In,check_Out):

        url = f'https://zh.hotels.com/search.do?destination-id=606535&q-check-in={check_In}&q-check-out={check_Out}&q-rooms=1&q-room-0-adults=1&q-room-0-children=0&sort-order=BEST_SELLER'
        # print(url)
        r = requests.get(url)
        soup = BeautifulSoup(r.content,'html.parser')
        return soup

def transfer_Soup(soup):
    hotels = soup.findAll('div', attrs = {'class':'_3NQzWW'}) 
    for row in hotels:

            hotel_Name = row.find('h2', attrs = {'class':'_3zH0kn'}).text
            print(hotel_Name)
            hotel_Level = row.find('span', attrs = {'class':'_2dOcxA'}).text
            print(hotel_Level)
            hotel_Location = row.find('p', attrs = {'class':'_2oHhXM'}).text
            print(hotel_Location)
            hotel_Price = row.find('span', attrs = {'class':'_2R4dw5'}).text
            print(hotel_Price)
            try:
                hotel_Quantity = row.find('div', attrs = {'class':'_2b3ymT'}).text
                print(hotel_Quantity)

            except:
                hotel_Quantity = '任何時間'
                print(hotel_Quantity)
        
            try:
                hotel_Discount = row.find('div', attrs = {'class':'lZh1gY'}).text
                print(hotel_Discount+'\n')
            except:
                hotel_Discount = ''
                print(hotel_Discount)

menu()
