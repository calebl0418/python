import requests
from bs4 import BeautifulSoup
import pandas as pd

def menu(): 
    choice = '0'
    while choice == '0':
        print('全新JobsDB的AI技術，在你登入後即幫你發掘適合好工，節省篩選時間，搵工成功率高4倍！ 即上JobsDB搵好工，發掘更多可能！ 登入用AI搵工，效率高6倍。超多職位空缺。貼地職場趨勢 盡在JobsDB。香港No.1 求職網站。全新JobsDB搵工成功率高4倍。')
        print("[1] Junior Programmer") 
        print("[2] Junior Data Analyst")
        choice = input ("Please make a choice: ")

    if choice == '1': 
        transferSoup(juniorProgrammer())
       
    elif choice == '2': 
        transferSoup(juniorAnalyst())

    else:
        print("I don't understand your choice.") 

def juniorProgrammer():
        url = f'https://hk.jobsdb.com/hk/search-jobs/junior-programmer/0'
        r = requests.get(url)
        soup = BeautifulSoup(r.content,'html.parser')
        return soup

def juniorAnalyst():
        url = f'https://hk.jobsdb.com/hk/search-jobs/junior-data-analyst/0'
        r = requests.get(url)
        soup = BeautifulSoup(r.content,'html.parser')
        return soup  

def transferSoup(soup):
    divs = soup.findAll('div',class_='sx2jih0 zcydq84g zcydq83g zcydq86g zcydq85g zcydq81w zcydq82o zcydq8cg zcydq8c4')
    for item in divs:
        title = item.find('div',class_='sx2jih0 _2j8fZ_0 sIMFL_0 _1JtWu_0').text
        # print(title)
        try:
            company = item.find('span',class_='sx2jih0 zcydq82c _18qlyvc0 _18qlyvcv _18qlyvc1 _18qlyvc8').text.strip()
            # print(company)
        except:
            company = 'Null'
            # print(company)
        try:
            location = item.find('span',class_='sx2jih0 zcydq82c zcydq8r iwjz4h0').text
            # print(location)
        except:
            location = 'Empty'
            # print(location)
        time = item.find('span',class_='sx2jih0 zcydq82c _18qlyvc0 _18qlyvcx _18qlyvc1 _18qlyvc6').text
        # print(time)
   
        jobs = { 
        'Postition': title,
        'Company': company,
        'Location':location,
        'Time' :time
        }
        jobList.append(jobs)
    return jobList

jobList = []
menu()

df = pd.DataFrame(jobList)
print(df.head())
df.to_csv('JobsDB.csv')